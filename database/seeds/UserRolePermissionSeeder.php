<?php

use App\Permission;
use App\Role;
use App\User;
use Illuminate\Database\Seeder;

class UserRolePermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();
        Role::truncate();
        Permission::truncate();
        DB::table('permission_role')->truncate();
        DB::table('role_user')->truncate();

        $userAdmin = User::create(['name' => 'Admin', 'email' => 'admin@laravel.com', 'password' => Hash::make('password')]);

        $roleAdmin       = Role::create(['name' => 'admin']);
        $roleEditor      = Role::create(['name' => 'editor']);
        $roleContributor = Role::create(['name' => 'contributor']);

        $postCreatePermission = Permission::create(['name' => 'post/create']);

        $userAdmin->roles()->sync([$roleAdmin->id, $roleEditor->id, $roleContributor->id]);

        $roleContributor->permissions()->save($postCreatePermission);
    }
}
