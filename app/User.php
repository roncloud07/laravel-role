<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\Access\Authorizable;

class User extends Model implements AuthenticatableContract,
AuthorizableContract,
CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public function isRole($role)
    {
        return $this->role == $role;
    }

    public function hasRole($role)
    {
        return $this->roles->where('name', $role)->count() > 0;
    }

    public function hasPermission($permission)
    {
        $roles       = $this->roles;
        $permissions = collect();

        foreach ($roles as $role) {
            $permissions = $permissions->merge($role->permissions);
        }

        return $permissions->where('name', $permission)->count() > 0;
    }

    public function roles()
    {
        return $this->belongsToMany('App\Role');
    }
}
