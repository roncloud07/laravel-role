<?php

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => 'auth'], function () {
    Route::get('admin', ['middleware' => 'role:admin', function () {
        return 'Hi, Admin!';
    }]);

    Route::get('editor', ['middleware' => 'role:editor', function () {
        return 'Hi, Editor!';
    }]);

    Route::get('contributor', ['middleware' => 'role:contributor', function () {
        return 'Hi, Contributor!';
    }]);

    Route::get('dashboard', function () {
        return 'Dashboard';
    });

    Route::group(['middleware' => 'permission'], function () {
        Route::get('post/create', function () {
            return 'Create Post';
        });
    });

});

Route::get('auth/login', function () {
    return view('auth.login');
});

Route::post('auth/login', function (Illuminate\Http\Request $request) {
    extract($request->input());

    if (Auth::attempt(['email' => $email, 'password' => $password])) {
        return redirect('dashboard');
    } else {
        return redirect('auth/login');
    }

});
